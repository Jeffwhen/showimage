import fetch from 'isomorphic-fetch'
import url from 'url';
import querystring from 'querystring';

export const REQUEST_POSTS = 'REQUEST_POSTS';
export const RECEIVE_POSTS = 'RECEIVE_POSTS';
export const SELECT_DIR = 'SELECT_DIR';
export const SELECT_API = 'SELECT_API';
export const SHOW_MODAL = 'SHOW_MODAL';
export const SELECT_ROW_LEN = 'SELECT_ROW_LEN';

export const selectDir = dir => ({type: SELECT_DIR, dir});
export const selectAPI = api => ({type: SELECT_API, api});
export const selectRowLen = rowLen => ({type: SELECT_ROW_LEN, rowLen});
const requestPosts = page => ({type: REQUEST_POSTS, page});

const receivePosts = (page, json) => ({
  type: RECEIVE_POSTS,
  page,
  count: json.end,
  posts: json.data.map(d => ({
    url: d.image_file,
    box: d.box,
    title: d.name,
    size: d.size
  }))
});

const fetchImages = page => (dispatch, getState) => {
  dispatch(requestPosts(page));
  let {api, dir} = getState();
  let urlObj = url.parse(api);
  let query = {...querystring.parse(urlObj.query), dir, num: page};
  const {host, pathname} = urlObj;
  api = url.format({host, pathname, query});
  return fetch(api, {credentials: 'same-origin'}).then(
    response => response.json()
  ).then(
    json => dispatch(receivePosts(page, json))
  );
};

export const fetchImagesIfNeeded = page => (dispatch, getState) => {
  if (!getState().fetching) {
    return dispatch(fetchImages(page));
  } else {
    return Promise.resolve();
  }
};

export const IMAGE_DETAIL_MODAL = 'IMAGE_DETAIL_MODAL';
export const showModal = (modalType, modalProps) => ({
  type: SHOW_MODAL, modalType, modalProps
});
