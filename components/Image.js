import React from 'react';
import PropTypes from 'prop-types';

let thumbWidth = 200;
let thumbHeight = 120;
let thumbStyle = {
  width: `${thumbWidth}px`,
  height: `${thumbHeight}px`,
  display: 'inline-block'
}
const Image = ({
  url, box, title, size, onClick
}) => {
  const bit = 10;
  const retreat = x => (x - bit > 0 ? x - bit : 0);
  const expand = (x, l) => (x + bit < l ? x + bit : l);

  let origBox = box;
  let newBox = {
    x: retreat(box.x),
    y: retreat(box.y),
    w: expand(box.w, size.width),
    h: expand(box.h, size.height)
  };

  let boxRatio = newBox.w / newBox.h;
  let thumbRatio = thumbWidth / thumbHeight;
  let scale = null;
  const dualExp = (off, len, div) => {
    off -= div;
    let overflow = 0;
    if (off < 0) {
      overflow = -off;
      off = 0;
    }
    len += overflow + div;
    return {off, len};
  };
  if (boxRatio < thumbRatio) {
    // Full height
    let newW = thumbRatio * newBox.h;
    let div = (newW - newBox.w) / 2;
    ({off: newBox.x, len: newBox.w} = dualExp(newBox.x, newBox.w, div));
    scale = thumbHeight / newBox.h;
  } else {
    // Full width
    let newH = newBox.w / thumbRatio;
    let div = (newH - newBox.h);
    ({off: newBox.y, len: newBox.h} = dualExp(newBox.y, newBox.h, div));
    scale = thumbWidth / newBox.w;
  }

  newBox = {
    x: newBox.x * scale,
    y: newBox.y * scale,
    w: newBox.w * scale,
    h: newBox.h * scale
  };
  let newImgWidth = size.width * scale;
  let newImgHeight = size.height * scale;
  let imgStyle = {
    top: `-${newBox.y / thumbHeight * 100}%`,
    left: `-${newBox.x / thumbWidth * 100}%`,
    width: `${newImgWidth / thumbWidth * 100}%`,
    height: `${newImgHeight / thumbHeight * 100}%`,
    position: 'relative'
  };

  let rectStyle = {
    position: 'absolute',
    width: `${origBox.w / size.width * 100}%`,
    left: `${origBox.x / size.width * 100}%`,
    height: `${origBox.h / size.height * 100}%`,
    top: `${origBox.y / size.height * 100}%`
  };
  let rect = (
    <div className="tag-rect" title={title} style={rectStyle}></div>
  );

  return (
    <div className="thumbnail" style={thumbStyle}>
      <div className="image-container" style={imgStyle}
        onClick={e => {
            onClick && onClick({url, box, size, title})
          }}
      >
        <img className="thumbnail-image" src={url}/>
        <div className="rects-block">{rect}</div>
      </div>
    </div>
  );
};

export const ImageProptypes = {
  url: PropTypes.string.isRequired,
  box: PropTypes.shape({
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
    w: PropTypes.number.isRequired,
    h: PropTypes.number.isRequired
  }).isRequired,
  size: PropTypes.shape({
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired
  }),
  title: PropTypes.string,
  onClick: PropTypes.func
};

Image.propTypes = ImageProptypes;

export default Image;
