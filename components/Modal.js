import React from 'react';
import PropTypes from 'prop-types';
import {Modal as BTModal, Button} from 'react-bootstrap';

const Modal = ({
  showModal, url, box, title, size, closeModal, onClick
}) => {
  if (!showModal) {
    return null;
  };
  let rectStyle = {
    position: 'absolute',
    width: `${box.w / size.width * 100}%`,
    left: `${box.x / size.width * 100}%`,
    height: `${box.h / size.height * 100}%`,
    top: `${box.y / size.height * 100}%`
  };
  let imgStyle = {
    position: 'relative',
    width: '100%',
    top: 0,
    left: 0
  };
  let rect = (
    <div className="tag-rect" title={title} style={rectStyle}></div>
  );

  return (
    <BTModal show={true} onHide={closeModal}>
      <BTModal.Header><BTModal.Title>
        详情
      </BTModal.Title></BTModal.Header>
      <BTModal.Body className="image-detail">
        <p>{title}</p>
        <div className="image-thumbnail">
          <div className="image-container" style={imgStyle}>
            <img className="thumbnail-image" src={url}/>
            <div className="rects-block">{rect}</div>
          </div>
        </div>
      </BTModal.Body>
      <BTModal.Footer>
        <Button onClick={closeModal}>确定</Button>
      </BTModal.Footer>
    </BTModal>
  );
};

Modal.propTypes = {
  url: PropTypes.string,
  box: PropTypes.shape({
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
    w: PropTypes.number.isRequired,
    h: PropTypes.number.isRequired
  }),
  size: PropTypes.shape({
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired
  }),
  title: PropTypes.string,
  closeModal: PropTypes.func
};
export default Modal;
