import Image, {ImageProptypes} from '../containers/Image';

import React from 'react';
import PropTypes from 'prop-types';

class ImageList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {page: null};
  }
  handlePageInputChange(e) {
    let v = parseInt(e.target.value);
    this.setState({
      page: v
    });
  }
  handlePageInput(e) {
    e.stopPropagation();
    e.preventDefault();
    let pageElem = e.target.page;
    pageElem.blur();
    let page = parseInt(pageElem.value);
    if (!Number.isNaN(page) && page >= 0 && page < this.props.count) {
      this.props.selPage(page - 1);
    }
    this.setState({
      page: null
    });
  }
  render() {
    let {images, page, count, selPage, rowLen} = this.props;
    let rows = [];
    for (let i = 0; i < Math.ceil(images.length / rowLen); i++) {
      let start = i * rowLen;
      rows.push(
        <div className="row" key={i}>
          {images.slice(start, start + rowLen).map((image, index) => (
             <Image key={index} {...image} />
           ))}
        </div>
      );
    }
    return (
      <div className="full-container">
        {rows}
        <div className="row">
          <button
            onClick={page - 1 >= 0 ? e => (selPage(page - 1)) : null}
          >上一页</button>
          <form onSubmit={this.handlePageInput.bind(this)} className="page">
            <input className="page-num" name="page"
              value={this.state.page || page + 1}
              onChange={this.handlePageInputChange.bind(this)}
            />
          </form>
          <span className="page-num">/{count}</span>
          <button
            onClick={page + 1 < count ? e => (selPage(page + 1)) : null}
          >下一页</button>
        </div>
      </div>
    );
  }
}

ImageList.propTypes = {
  images: PropTypes.arrayOf(PropTypes.shape(ImageProptypes)).isRequired,
  page: PropTypes.number.isRequired,
  count: PropTypes.number.isRequired,
  selPage: PropTypes.func.isRequired
}

export default ImageList;
