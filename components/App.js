import React from 'react';
import PageImages from '../containers/PageImages';
import Modal from '../containers/Modal';

const App = () => (
  <div className="show-image-container">
    <PageImages />
    <Modal />
  </div>
);

export default App;
