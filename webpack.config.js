const path = require('path')
const webpack = require('webpack')

module.exports = {
  entry: {
    showimage: ['./index.js'],
    vendor: ['react', 'react-dom', 'redux', 'react-redux', 'react-bootstrap']
  },
  output: {
    filename: '[name]-bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [{
      test: /\.css$/,
      use: ['style-loader', 'css-loader']
    }, {
      test: /\.jsx?$/,
      loader: 'babel-loader',
      query: {
        presets: ['es2015', 'react', 'stage-2']
      },
      exclude: /(node_modules|bower_components)/
    }, {
      test: /\.(png|svg|jpg|gif)$/,
      use: [
        {
          loader: 'file-loader',
          query: {
            name: '/gulp-tutorial/dist/[hash].[ext]'
          }
        }
      ]
    }]
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor', filename: 'vendor.js'
    })
  ]
};
