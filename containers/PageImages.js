import ImageList from '../components/ImageList';
import { connect } from 'react-redux';

import { fetchImagesIfNeeded } from '../actions';

const mapStateToProps = state => ({
  images: state.images,
  page: state.page,
  count: state.count,
  rowLen: state.rowLen
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  selPage: (n) => {
    dispatch(fetchImagesIfNeeded(n));
  }
});

const PageImages = connect(
  mapStateToProps, mapDispatchToProps
)(ImageList);

export default PageImages;
