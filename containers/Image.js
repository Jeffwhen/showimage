import Image from '../components/Image';
import { connect } from 'react-redux';

import { showModal, IMAGE_DETAIL_MODAL } from '../actions';

const mapDispatchToProps = (dispatch, ownProps) => ({
  onClick: (props) => (dispatch(showModal(
    IMAGE_DETAIL_MODAL, props
  )))
});

const ImageC = connect(
  undefined, mapDispatchToProps
)(Image);

export default ImageC;
