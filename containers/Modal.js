import Modal from '../components/Modal';
import { connect } from 'react-redux';

import { showModal } from '../actions';

const mapStateToProps = state => ({
  ...state.modal.modalProps,
  showModal: state.modal.modalType
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  closeModal: () => (dispatch(showModal(null)))
});

const ModalC = connect(
  mapStateToProps, mapDispatchToProps
)(Modal);

export default ModalC;
