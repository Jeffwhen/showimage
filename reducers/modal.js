import { SHOW_MODAL } from '../actions';

const initialState = {
  modalType: null,
  modalProps: {}
};

function modal(state=initialState, action) {
  switch (action.type) {
    case SHOW_MODAL:
      return {
        modalType: action.modalType,
        modalProps: action.modalProps
      };
    default:
      return state;
  }
}

export default modal;
