import { combineReducers } from 'redux';

import {
  REQUEST_POSTS,
  RECEIVE_POSTS,
  SELECT_DIR,
  SELECT_API,
  SELECT_ROW_LEN
} from '../actions';
import modal from './modal';

const images = (state=[], action) => {
  switch (action.type) {
    case RECEIVE_POSTS:
      return action.posts.slice(0);
    default:
      return state;
  }
};

const page = (state=0, action) => {
  switch (action.type) {
    case RECEIVE_POSTS:
    case REQUEST_POSTS:
      return action.page;
    default:
      return state;
  }
};

const count = (state=0, action) => {
  switch (action.type) {
    case RECEIVE_POSTS:
      return action.count;
    default:
      return state;
  }
};

const rowLen = (state=5, action) => {
  switch (action.type) {
    case SELECT_ROW_LEN:
      return action.rowLen;
    default:
      return state;
  }
};

const defaultAPI = '/admin/task/getLabelTxt';
const api = (state=defaultAPI, action) => {
  switch (action.type) {
    case SELECT_API:
      return action.api;
    default:
      return state;
  }
};

const dir = (state='', action) => {
  switch (action.type) {
    case SELECT_DIR:
      return action.dir;
    default:
      return state;
  }
};

const fetching = (state=false, action) => {
  switch (action.type) {
    case REQUEST_POSTS:
      return true;
    case RECEIVE_POSTS:
      return false;
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  images, page, count, dir, api, fetching, modal, rowLen
});

export default rootReducer;
