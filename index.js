'use strict';

import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import {
  selectDir, selectAPI,
  selectRowLen, fetchImagesIfNeeded
} from './actions';
import reducer from './reducers';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger'

import App from './components/App';

import './showimage.css';

const loggerMiddleware = createLogger();
const store = createStore(
  reducer, applyMiddleware(thunkMiddleware, loggerMiddleware)
);

const selDir = (dir) => store.dispatch(selectDir(dir));
const selAPI = (api) => store.dispatch(selectAPI(api));
const selRowLen = (rowLen) => store.dispatch(selectRowLen(rowLen));
const nFetchImagesIfNeeded = (n) => store.dispatch(fetchImagesIfNeeded(n));
const nextPage = (step=1) => {
  let page = store.getState().page;
  nFetchImagesIfNeeded(page + step);
};

window.showimage = {
  selDir, selAPI, nextPage, selRowLen,
  fetchImages: nFetchImagesIfNeeded
};

render(
  <Provider store={ store }>
    <App />
  </Provider>,
  document.getElementById('modal-converter')
);
