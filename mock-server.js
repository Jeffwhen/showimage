"use strict";

var port = normalizePort(process.env.PORT || 7334);
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

app.use(bodyParser.json());
app.use(function (req, res, next) {
  console.log("Request to path " + req.path + "\n");
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Max-Age", 86400);
  next();
});

var resp = {"end":10,"code":1,"data":[{"box":{"y":0,"x":0,"w":63,"h":72},"name":"plane","blur":false,"image_file":"http://imgserver.yunshitu.cn/task/detect/ZhangFan/RS/alpha/SubImages/JL101A_PMS_20160515010800_000008725_201_0013_001_L1_PAN_1_2_2664_2608_2919_2863.jpg","id":["2"],"size":{"width":256,"height":256}},{"box":{"y":0,"x":108,"w":79,"h":85},"name":"plane","blur":false,"image_file":"http://imgserver.yunshitu.cn/task/detect/ZhangFan/RS/alpha/SubImages/JL101A_PMS_20160515010800_000008725_201_0013_001_L1_PAN_1_2_3760_3224_3999_3479.jpg","id":["2"],"size":{"width":240,"height":256}}]};

app.get("/admin/task/getLabelTxt", function (request, response) {
  var data = [];
  for (var i = 0; i < 10; i++) {
    data.push(resp['data'][i % 2]);
  }
  resp['data'] = data;
  setTimeout(() => {
    response.json(resp);
  }, 1000);
});

app.listen(port, function () {
  console.log('Show image mock server @' + port);
});
